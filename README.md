# docker-packer

This creates a docker image with packer based on the registry.gitlab.com/naturalis/lib/ansible/docker-ansible:stable image

## Current versions

packer 1.11.2
Ansible: 10.1.0
Ansible-core: 2.17.1
Ansible-hashivault-module: 4.6.6
Ansible-lint: 24.7
