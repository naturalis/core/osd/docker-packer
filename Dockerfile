FROM registry.gitlab.com/naturalis/lib/ansible/docker-ansible:10.1.0

LABEL maintainer="Infra Team Naturalis"

ENV DEBIAN_FRONTEND=noninteractive
ENV TZ=Etc/UTC

RUN apt update && \
  apt install wget && \
  wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | tee /usr/share/keyrings/hashicorp-archive-keyring.gpg && \
  echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com focal main" | tee /etc/apt/sources.list.d/hashicorp.list && \
  apt update && \
  apt install packer=1.11.2-1 && \
  rm -rf /var/cache/apt && \
  apt-get clean
